# build it: docker build -t opstudio/ansible-gc .
FROM ubuntu:16.04
LABEL maintainer "a.ageyev@gmail.com"
WORKDIR /etc/ansible

ENV DEBIAN_FRONTEND noninteractive
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y  --assume-yes apt-utils
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:ansible/ansible
RUN apt-get update
RUN apt-get install -y ansible git-core

RUN apt-get install -y curl apt-transport-https
RUN echo "deb https://packages.cloud.google.com/apt cloud-sdk-xenial main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN apt-get update && apt-get install -y google-cloud-sdk google-cloud-sdk-app-engine-python
RUN apt-get install -y python-pip python-dev build-essential
RUN pip install --upgrade pip
RUN pip install apache-libcloud

ENTRYPOINT ["/bin/bash"]
